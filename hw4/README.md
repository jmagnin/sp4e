# HW4

## Results

### Ex. 1,2 and 3 :

The binding to the required code is done in the `src/pypart.cc` file.  

To ensure that the proper management of `Compute` objects, we bind using :

- `py::return_value_policy::reference` as a return policy;
- `std::shared_pointer` as object holders.

### Ex. 4 : Mercury Trajectory

After running the simulation by using the following command from `hw4` :  

```bash
./build/main.py  365 1 ./src/init.csv planet 1
```

We get one dump file per day in the `dumps` folder. Using paraview to vizualize the planets,
we observe that mercury (in pink) drifts straight away instead of staying on oits orbit
as expected :

![mercury drifting](./img/mercury_drift.gif)

### Ex. 5 : Error on Mercury

The methods `readPositions` and `computeError` are implementefd in `main.py`.  

Using the provided `trajectories` folder with the reference positions over time,
one can compute the error of position for each planet.  

The code simply run the simulation on 365 days and compare the mean square error
between Mercury and Earth :

```bash
./build/main.py 365 1 src/init.csv planet 1
The error for Mercury is : 165.09920425817882 AU.
The error for Earth is : 0.6340350691665866 AU.
```

We can immediately see that Mercury yields a huge error unlike Earth. 

### Ex. 6 : Scaling velocity

The `generateInput` function is implemented along with 2 other function that it uses :
`readInitFile` and `writeInitFile`. A simple test by scaling a planet's velocity to 0
is made and confirm that it works.  With no velocity, Mercury plunges to the Sun which ends
up in sending it on an intersideral trip with the most ambitious gravity assist ever imagined.
The implemented functions `launchParticles` and `runAndComputeError` allow us to know the error
in this scenario.It results, after 365 days, in an enormous error :

``` bash
Mercury error if velocity scaled to 0 : 58558.577306783416 AU
```

Note that the `timestep` argument was added to the `launchParticles` and `runAndComputeError`
functions.


### Ex. 7 : Optimize and find Mercury's velocity

The correct initial velocity of Mercury is estimated by minimizing the error
with respect to the reference when scaling the velocity. To do so, a simple
lambda function based on `runAndComputeError` is gicen to `scipy.optimize.fmin`.
We then obtain the following result :  

```bash
Optimization terminated successfully.
         Current function value: 0.516509
         Iterations: 18
         Function evaluations: 37
Otimization of Mercury initial velocity :
        Optimal scale = 0.3989501953124994
        Error on 365 days after scaling = 0.516509186860567
```

This error is comparable to the error on Earth. Which means the velocity is correct.

Finally, we can use paraview to observe the trajectory of Mercury :

![mercury in orbit](./img/mercury_orbit.gif)

It is a success, Mercury stays in orbit ! We can even see the elliptical nature of its
orbit by noticing that its distance to the sun and its velocity changes significantly.

Finally, below is a plot of the error with respect to the scaling of Mercury velocity :

![Error for scale](img/error_for_scale.png)

We can see that the solution indeed lies on the minimum.

## Work repartition

### Part 1

- [x] Tim : Factory interface binding;
- [x] Tim : Compute binding;
- [x] Tim : Other classes binding;

### Part 2

- [x] Jo  : Ex. 4 trajectory visuals in the README;
- [x] Jo : Ex. 5 : compute error + results in README;
- [x] Jo  : Ex. 6 : Particle code from python;
- [x] Jo : Ex. 7 : do optimization + plots and give Mercury velocity;

### Hand-in

- [x] Jo : Code and doc review;
- [x] Jo : Write and review README;
- [x] Tim : Write comment on first 3 exercices in README.
- [x] Tim : Test functionality from new repo;

## How to run

### Dependancies

In order to successfully run the program, one needs :

- the FFTW3 library installed. To install it on your system,
see [the fftw webpage](http://fftw.org/);
- a Python 3 installation with modules :
    - numpy;
    - matplotlib;
    - argparse;
    - scipy;

### Build

Upon pulling, one should have the following content in the `hw4` folder :

```bash
tree -L 1
.
├── img
├── paraview
├── README.md
├── src
└── trajectories
```

In order to complete the folder, we must create two more folders :

```bash
mkdir build dumps
```

And we should have the following folders in `hw4` :

```bash
tree -L 1
.
├── build
├── dumps
├── img
├── paraview
├── README.md
├── src
└── trajectories
```

Before being able to run, the three git submodules shall be populated. To do so :

```bash
cd ../third_party/
git submodule update --init googletest
git submodule update --init eigen
git submodule update --init pybind11
cd ../hw4/
```

The submodule should now be ready. Note that they are linked in the source by symbolc links.
One can check that they work by issuing (from `hw4`) :

```bash
ls -la src/ | grep "../../"
lrwxrwxrwx 1 jonathan jonathan     24 19 déc 13:12 eigen -> ../../third_party/eigen//
lrwxrwxrwx 1 jonathan jonathan     29 19 déc 13:12 googletest -> ../../third_party/googletest//
lrwxrwxrwx 1 jonathan jonathan     27 19 déc 13:12 pybind11 -> ../../third_party/pybind11//
```

Then, the project can be built by using the following commands from `hw4` :

```bash
cmake src/ -B build/
make -C build/
```

The project should build successfully.

Fanally, to execute the unit tests, one can run :

```bash
./build/test_fft
./build/test_kepler
```

Both tests should be successful and end with the `[  PASSED  ]` statement.

### Run

From `hw4` always, the program can be run by executing `./build/main.py` (works only if
python3 is installed) with the following syntax :

```bash
./build/main.py [-h] [-o OPTIMIZE] nsteps freq filename particle_type timestep
```

It will run the ported C++ code as expected. If using the `-o` flag, then the programm will
optimize the Mercury velocity to fit the reference trajectory. This argument requires the
optimization initial guess.

#### Example : Run the program normaly

By running, for example :

```bash
./build/main.py 365 1 ./src/init.csv planet 1
```

The program does not print anything, but it fills the `dumps` folder with the steps
of the planet simulation. In this case, we will generate the solar system over 1 year
with the flawed Mercury velocity.

#### Example : Run the program to optimize Mercury velocity

By running, for example :

```bash
./build/main.py 365 1 ./src/init.csv planet 1 -o 2.5
```

The will print :

```bash
The error for Mercury is : 165.09920425817882 AU.
The error for Earth is : 0.6340350691665866 AU.
Mercury error if velocity scaled to 0 : 58558.577306783416 AU
Optimization terminated successfully.
         Current function value: 0.516509
         Iterations: 18
         Function evaluations: 37
Otimization of Mercury initial velocity :
        Optimal scale = 0.3989501953124994
        Error on 365 days after scaling = 0.516509186860567 AUh

```

It compares the error for Mercury and for Earth before minimizing it relative to the reference.
It then run the simulation again and shows that the error for Mercury is comparable to Earth.
It displays a plot of the error over the scale and shows where the solution lies. The `dumps`
folder contains the steps of the simulation of the optimized version. The `build` folder will also
contain the `init_scaled.csv` file that ontains the initial state with the Mercury velocity scaled.

### Use output files

The content of the `dumps` folder can, as usual, be used to show the particles on paraview. The Results
section contains some examples.
