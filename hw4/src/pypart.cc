#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "compute_temperature.hh"
#include "compute_gravity.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  py::class_<System>(m, "System");

  py::class_<SystemEvolution>(m, "SystemEvolution")
	      .def("evolve", &SystemEvolution::evolve, py::return_value_policy::reference)
        .def("setNSteps", &SystemEvolution::setNSteps)
	      .def("setDumpFreq", &SystemEvolution::setDumpFreq)
        .def("addCompute", &SystemEvolution::addCompute, py::return_value_policy::reference)
        .def("getSystem", &SystemEvolution::getSystem, py::return_value_policy::reference);
  
  py::class_<ParticlesFactoryInterface, std::shared_ptr<ParticlesFactoryInterface>>(m, "ParticlesFactoryInterface")
        .def("getInstance", &ParticlesFactoryInterface::getInstance, py::return_value_policy::reference)
        .def_property_readonly("system_evolution", &ParticlesFactoryInterface::getSystemEvolution, py::return_value_policy::reference);
  
  py::class_<MaterialPointsFactory, std::shared_ptr<MaterialPointsFactory>, ParticlesFactoryInterface>(m, "MaterialPointsFactory")
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
	        (&MaterialPointsFactory::createSimulation<py::function>), py::return_value_policy::reference)
        .def("createParticle", &MaterialPointsFactory::createParticle)
        .def("getInstance", &MaterialPointsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, std::shared_ptr<PingPongBallsFactory>, ParticlesFactoryInterface>(m, "PingPongBallsFactory")
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
	        (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
        .def("createParticle", &PingPongBallsFactory::createParticle)
        .def("getInstance", &PingPongBallsFactory::getInstance, py::return_value_policy::reference);

  py::class_<PlanetsFactory, std::shared_ptr<PlanetsFactory>, ParticlesFactoryInterface>(m, "PlanetsFactory")
        .def("createSimulation", py::overload_cast<const std::string&, Real, py::function>
	        (&ParticlesFactoryInterface::createSimulation<py::function>), py::return_value_policy::reference)
        .def("createParticle", &PlanetsFactory::createParticle)
        .def("getInstance", &PlanetsFactory::getInstance, py::return_value_policy::reference);

  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute");
  
  py::class_<ComputeInteraction, std::shared_ptr<ComputeInteraction>, Compute>(m, "ComputeInteraction");
  py::class_<ComputeVerletIntegration, std::shared_ptr<ComputeVerletIntegration>, Compute>(m, "ComputeVerletIntegration")
        .def(py::init<Real>())
        .def("setDeltaT", &ComputeVerletIntegration::setDeltaT)
        .def("compute", &ComputeVerletIntegration::compute)
        .def("addInteraction", &ComputeVerletIntegration::addInteraction);
  
  py::class_<CsvWriter, std::shared_ptr<CsvWriter>, Compute>(m, "CsvWriter")
        .def(py::init<const std::string &>())
        .def("write", &CsvWriter::write)
        .def("compute", &CsvWriter::compute);

  py::class_<ComputeTemperature, std::shared_ptr<ComputeTemperature>, Compute>(m, "ComputeTemperature")
        .def(py::init<>(), py::return_value_policy::reference)
        .def("compute", &ComputeTemperature::compute)
        .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::setConductivity)
        .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::setCapacity)
        .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::setDensity)
        .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::setL)
        .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::setDeltat);

  py::class_<ComputeGravity, std::shared_ptr<ComputeGravity>, ComputeInteraction>(m, "ComputeGravity")
        .def(py::init<>(), py::return_value_policy::reference)
        .def("setG", &ComputeGravity::setG, py::return_value_policy::reference)
        .def("compute", &ComputeGravity::compute, py::return_value_policy::reference);
}
