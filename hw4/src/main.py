#!/bin/env python3

import sys
import argparse
import numpy as np
import csv
import os
import matplotlib.pyplot as plt
import scipy
from pypart import MaterialPointsFactory, ParticlesFactoryInterface
from pypart import PingPongBallsFactory, PlanetsFactory
from pypart import CsvWriter
from pypart import ComputeTemperature
from pypart import ComputeGravity
from pypart import ComputeVerletIntegration


def main(nsteps, freq, filename, particle_type, timestep):

    # init ParticleFactory instance
    if particle_type == "planet":
        PlanetsFactory.getInstance()
    elif particle_type == "ping_pong":
        PingPongBallsFactory.getInstance()
    elif particle_type == "material_point":
        MaterialPointsFactory.getInstance()
    else:
        print("Unknown particle type: ", particle_type)
        sys.exit(-1)

    # simply run the code as expected
    if not optimize :
        launchParticles(filename, nsteps, freq, timestep)
        
    # optimize Mercury velocity
    else :
        # Ex 4 and 5 : compute error on Earth and Mercury and compare
        launchParticles(filename, nsteps, freq, timestep)
        pos = readPositions("mercury", "./dumps")
        pos_ref = readPositions("mercury", "./trajectories")
        err_mercury = computeError(pos, pos_ref)

        pos = readPositions("earth", "./dumps")
        pos_ref = readPositions("earth", "./trajectories")
        err_earth = computeError(pos, pos_ref)

        print("The error for Mercury is : " + str(err_mercury) + " AU.")
        print("The error for Earth is : " + str(err_earth) + " AU.")

        # Ex 6 : scale velocity of a planet
        generateInput(0.0, "mercury", filename, "build/init_scaled.csv")
        err = runAndComputeError(0.0, "mercury", filename, nsteps, freq, timestep)
        print("Mercury error if velocity scaled to 0 : " + str(err) + " AU")

        # Ex 7 : optimize velocity
        cf = lambda x : runAndComputeError(x[0], "mercury", filename, nsteps, freq, timestep)
        minimum = scipy.optimize.fmin(cf, 1.0)
        opt_err = cf(minimum)
        print("Otimization of Mercury initial velocity :")
        print("\tOptimal scale = " + str(minimum[0]))
        print("\tError on 365 days after scaling = " + str(opt_err) + " AU")

        # plot error
        scales = np.linspace(0.1, 3, 100)
        errs = np.array([cf([x]) for x in scales])
        ax = plt.figure().add_subplot()
        ax.plot(scales, errs, label="error")
        ax.scatter(minimum, opt_err, marker="x", color="red", label="solution")
        ax.set_xlabel("Scale on Mercury velocity [-]")
        ax.set_ylabel("MEan square error on 365 days [AU]")
        ax.grid()
        ax.legend()
        plt.title("Error on Mercury trajectory")

        plt.show()

def createComputes(self, timestep):

        if particle_type == "planet":

            try:
                compute_grav = ComputeGravity()
                compute_verlet = ComputeVerletIntegration(timestep)

                G = 6.67384e-11  # m^3 * kg^-1 * s^-2
                UA = 149597870.700  # km
                earth_mass = 5.97219e24  # kg
                G /= (UA * 1e3)**3  # UA^3 * kg^-1 * s^-2
                G *= earth_mass    # UA^3 * earth_mass^-1 * s^-2
                G *= (60*60*24)**2  # UA^3 * earth_mass^-1 * day^-2

                compute_grav.setG(G)
                compute_verlet.addInteraction(compute_grav)
                self.system_evolution.addCompute(compute_verlet)

            except Exception as e:
                help(compute_grav)
                raise e

        elif particle_type == 'material_point':

            try:
                compute_temp = ComputeTemperature()
                compute_temp.conductivity = 1
                compute_temp.L = 2
                compute_temp.capacity = 1
                compute_temp.density = 1
                compute_temp.deltat = 1
                self.system_evolution.addCompute(compute_temp)
            except Exception as e:
                help(compute_temp)
                raise e


def readPositions(planet_name : str, directory: str) -> np.array :
    """
    Reads the given directory and load the required planet's
    trajectory.

    :param planet_name :    name of the planet (lower case)
    :param directory :      directory in which the files are

    :returns    Nx3 np array containing the N steps of the 3D position
                of the planet.
    """
    # get all filenames from directory ad order them in steps
    filenames = next(os.walk(directory), (None, None, []))[2] 
    filenames = sorted(filenames)

    # read all files for the given planet and store the positions
    positions = []
    for file in filenames :
        f = open(os.path.join(directory, file))
        reader = csv.reader(f,  delimiter=" ")
        for row in reader :
            try :
                row.remove("")
            except :
                pass
            if row[-1] == planet_name :
                positions.append(np.array([float(row[0]), float(row[1]), float(row[2])]))
                break
        
    # close file
    f.close()

    # check if positions were founds
    if not positions :
        raise ValueError("Planet name : \"" + planet_name + "\" not found.")

    # return positions
    return np.array(positions)

def computeError(positions: np.array, positions_ref: np.array) -> float :
    """
    Compute the mean square error between the given trajectory and reference
    trajectory.

    :param positions :      Nx3 array of N 3D positions
    :param positions_ref :  Nx3 array of N 3D positions of reference

    :returns    mean squared error defined as :

                E = sqrt( sum(|X - X_ref|^2) )
    """
    err = np.sqrt(np.sum(np.square(positions - positions_ref), axis=None))
    return err

def readInitFile(filename: str) -> dict :
    """
    read the given initialization file and returns a dictionary with planets names as keys.

    :param filename :   init file name, including path.

    :returns :  dictionary containing all data with planet names as keys
    """
    # open file
    try :
        f = open(filename)
    except :
        raise ValueError("Could not open file : " + filename)

    # read file and store data for each row
    planets = dict()
    reader = csv.reader(f,  delimiter=" ")
    for row in reader :
        try :
            row.remove("")
        except :
            pass

        # skip header
        if row[0][0] == "#" :
            continue

        # store values
        pos = np.array([float(row[0]), float(row[1]), float(row[2])])
        vel = np.array([float(row[3]), float(row[4]), float(row[5])])
        force = np.array([float(row[6]), float(row[7]), float(row[8])])
        mass = float(row[9])
        name = row[10]
        planets.update({name : {"pos" : pos, "vel" : vel, "force" : force, "mass" : mass}})

    # close file
    f.close()

    # return positions
    return planets

def writeInitFile(filename: str, planets: dict) -> None :
    """
    Write the given planet data in the desired CSV file space separated.

    :param filename :   init file name, including path.
    :param planets :    dictionary containing all data with planet names as keys
    """
    # open file
    try :
        f = open(filename, "w")
    except :
        raise ValueError("Could not open file : " + filename)

    # write header row
    writer = csv.writer(f,  delimiter=" ")
    writer.writerow(["#X", "Y", "Z", "VX", "VY", "VZ", "FX", "FY", "FZ", "mass", "name"])

    # write a row for each planet
    for name, data in planets.items() :
        pos = data["pos"].tolist()
        vel = data["vel"].tolist()
        force = data["force"].tolist()
        mass = data["mass"]
        writer.writerow(pos + vel + force + [mass] + [name])

    # close file
    f.close()

def generateInput(scale: float, planet_name: str, input_filename: str, output_filename: str) -> None :
    """
    Generates a new init file based on the given one but with the velocity of
    the desired planet scaled.

    :param scale :              scaling factor for the planet's velocity in the new file
    :param planet_name :        name of the planet to scale
    :param input_filename :     filename of the reference initialization file (incl path)
    :param output_filename :    filename of the resulting file (incl path)  
    """
    # read input file
    planets = readInitFile(input_filename)

    # scale velocity
    vel = planets[planet_name]["vel"]
    vel = vel * scale
    planets[planet_name].update({"vel" : vel})

    # write in output file
    writeInitFile(output_filename, planets)

def launchParticles(input: str, nb_steps: int, freq: int, timestep: float) -> None :
    """
    Function that launches the C++ particle code.
    The results are dumped in the "dumps" folder.

    :param input :      initialization file (incl path)
    :param nb_steps :   number of steps to run
    :param freq :       dump frequency
    :param timestep :   time step in correct units for simulation
    """
    # get factory
    factory = ParticlesFactoryInterface.getInstance()

    # create simulation setup
    evol = factory.createSimulation(input, timestep, createComputes)
    evol.setNSteps(nb_steps)
    evol.setDumpFreq(freq)

    # simulate
    evol.evolve()

def runAndComputeError(scale: float, planet_name: str, input: str, nb_steps: int, freq: int, timestep: float) -> float :
    """
    Executes the partivle code with a scaled init file and return the error relative to the reference
    trajectories.

    :param scale :          scaling factor for the planet's velocity in the new file
    :param planet_name :    name of the planet to scale
    :param input :          initialization file (incl path)
    :param nb_steps :       number of steps to run
    :param freq :           dump frequency
    :param timestep :   time step in correct units for simulation

    :returns :              mean square error over every timestep
    """
    # scale the velocity of the chosen planet
    generateInput(scale, planet_name, input, "build/init_scaled.csv")

    # run the simulation for a given scale
    launchParticles("build/init_scaled.csv", nb_steps, freq, timestep)

    # read the result
    pos = readPositions(planet_name, "dumps")

    # read reference
    ref_pos = readPositions(planet_name, "trajectories")

    # compute error and return
    return computeError(pos, ref_pos)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Particles code')
    parser.add_argument('nsteps', type=int,
                        help='specify the number of steps to perform')
    parser.add_argument('freq', type=int,
                        help='specify the frequency for dumps')
    parser.add_argument('filename', type=str,
                        help='start/input filename')
    parser.add_argument('particle_type', type=str,
                        help='particle type')
    parser.add_argument('timestep', type=float,
                        help='timestep')
    parser.add_argument('-o', '--optimize',
                        help='set flag to enable optimization of Mercury velocity. The startpoint scale is required')

    args = parser.parse_args()
    nsteps = args.nsteps
    freq = args.freq
    filename = args.filename
    particle_type = args.particle_type
    timestep = args.timestep
    optimize = args.optimize

    main(nsteps, freq, filename, particle_type, timestep)
