"""
General Minimal Residual Method
Exercice 2 of homework 1.
Implements the GMRES optimization method.
---
12.10.2023
---
jmagnin
ttuuva
"""

import numpy as np


def gmres(S_x, x0: np.array, max_iter: int = 100, eps: float = 1e-12, cb=None):
    """
    Custom implementation of the GMRES algorithm.
    Compute a basis of the (n + 1)-Krylov subspace of the matrix A.
    Source: https://en.wikipedia.org/wiki/Arnoldi_iteration
    parameters:
      S_x: functor
          0.5*x.T @ A @ x - x.T @ b

      x0: array_like
          [nx1] initial guess of the inverse problem

      max_iter: max number of iterations

      eps: absolute tolerance

      cb : callback function taking one argument which is the current iteration

    Returns
        x:          solution
        state :     True if convergence is reached, False otherwise
    """
    A, b = S_x.A, S_x.b
    m = max_iter
    e1 = np.zeros(m+1)
    e1[0] = 1

    # Initial residual guess
    r0 = b - A @ x0
    r0_norm = np.einsum('i,i', r0, r0)**0.5
    beta = r0_norm * e1
    h = np.zeros((m + 1, m))
    Q = np.zeros((A.shape[0], m + 1))

    # Normalize the input vecto, use it as the first Krylov vector
    Q[:, 0] = b / np.einsum('i,i', b, b) ** 0.5
    x = None
    for k in range(1, m + 1):

        v = np.einsum('ij,j->i', A, Q[:, k - 1])
        h[:, k - 1] = np.einsum('ji, j->i', Q.conj(), v)
        v = v - np.einsum('ij,j->i', Q, h[:, k - 1])
        h[k, k - 1] = np.einsum('i,i', v, v) ** 0.5

        # compute current xk
        y = np.linalg.pinv(h) @ beta
        x = x0 + Q[:, :m] @ y

        # callback
        if cb is not None:
            cb(x)

        if h[k, k - 1] > eps:  # Add the produced vector to the list, unless
            Q[:, k] = v / h[k, k - 1]
        else:  # If that happens, stop iterating.
            # convergence reached
            return x, True
        # did not converge
    return x, False
