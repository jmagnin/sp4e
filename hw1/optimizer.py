"""
Optimizer exercice of the homework 1.
Implements functions to return an optimizer and to
plot the iterations in the solution space.
---
12.10.2023
---
jmagnin
ttuuva
"""

import scipy
import numpy as np
import logging
import matplotlib.pyplot as plt
import argparse

import GMRES


class S:
    """
    Simple functor implementing the objective function
    """
    def __init__(self, A, b):
        self.A = A
        self.b = b

    def __call__(self, x):
        return 0.5*(x.T @ self.A  @ x) - x.T @ self.b


def optimize(S_x, method="BFGS", x0=np.array([0, 0])):
    """
    Creates an optimizer for a functor using the given method and startpoint.
    :param S_x:     functor of the mathematical function to optimize
    :param method:  "BFGS", "LMGRES" or "GMRES", optimization method
    :param x0:      startpoint, dimensions must be consistent with S_x arg
    :return xopt:   solution point as a np.array
    :return sopt:   value of unction at xopt
    :return iter:   iterations on the solution vector
    """
    # define callback and iterations
    iter = [x0]

    def iter_cb(xk):
        """
        Callback function for solvers. Stores each iteration
        solution vector in a list
        :param xk:
        :return: void
        """
        iter.append(xk)

    # Call appropriate optimizer
    if method == "BFGS":
        opt_res = scipy.optimize.minimize(S_x, x0, method=method, options={'gtol': 1e-9, 'disp': False}, callback=iter_cb)
        if not opt_res.success:
            logging.warning("Unsuccessful optimization.")
        else:
            logging.info("Convergence is reached.")
        logging.info(opt_res.message)
        xopt = opt_res.x
        sopt = opt_res.fun
    elif method == "LGMRES":
        x, info = scipy.sparse.linalg.lgmres(S_x.A, S_x.b, atol=1e-9, callback=iter_cb)
        m = "Optimization terminated successfully."
        if info < 0:
            logging.warning("Unsuccessful Optimization.")
            m = "Illegal input."
        elif info > 0:
            logging.warning("Unsuccessful optimization.")
            m = "Convergence not archived."
        else:
            logging.info("Convergence is reached.")
        logging.info(m)
        xopt = x
        sopt = S_x(x)
    elif method == "GMRES":
        x, state = GMRES.gmres(S_x, x0, 100, 1e-9, iter_cb)
        if not state:
            logging.warning("Unsuccessful Optimization.")
        else:
            logging.info("Convergence is reached.")
        xopt = x
        sopt = S_x(x)
    else:
        logging.error("Invalid optimization method.")
        raise AttributeError

    # return optimization result
    print(iter)
    return xopt, sopt, np.array(iter)


def display_opt(S_x: S, xlim: tuple, ylim: tuple, iter: np.array, title: str):
    """
    Displays the function to optimize and the iterations of the solver.
    Calling this function blocks the execution to display the figures.
    :param S_x:     functor of the function to optimize
    :param xlim:    tuple of plot limits in the x dimension
    :param ylim:    tuple of plot limits in the x dimension
    :param iter:    Array of each iteration in the solution space
    :param title :  str, desired title of the plot

    :return: void
    """
    # samples solution space around optimum
    x = np.linspace(xlim[0], xlim[1], num=1000)
    y = np.linspace(ylim[0], ylim[1], num=1000)
    mesh = np.array(np.meshgrid(x, y)).reshape(2, -1).T
    z = np.array([S_x(p) for p in mesh]).reshape(len(x), len(y))

    # draw surface
    ax = plt.figure().add_subplot(projection='3d')
    ax.view_init(elev=30, azim=120)
    xmesh, ymesh = np.meshgrid(x, y)
    ax.plot_surface(xmesh, ymesh, z, cmap="inferno", alpha=0.2)

    # draw contour
    ax.contour(x, y, z, colors="black")  # Plot contour curves

    # legends
    ax.set_title(title)
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Z")

    # draw iterations
    ax.plot(iter[:, 0], iter[:, 1], [S_x(i) for i in iter], linestyle="--", color="red", marker="o")

    # show
    plt.show()


def parse_args():
    """
    Parse the arguments when calling this script
    :return: args, the arguments
    """
    # Parsing the input of the program; A, b and method choice
    def parse_array(M):
        return np.asarray(np.matrix(M))

    parser = argparse.ArgumentParser(description='Program to solve Ax=b using GMRES or BFGS.')
    parser.add_argument('A', metavar='A', type=parse_array,
                        help='Matrix A, in this format: [[a00, a01, ... a0n]; \
                                                             [a10, a11, ... a1n]; \
                                                                    ... \
                                                             [an0, ..., ann]]')

    parser.add_argument('b', metavar='b', type=parse_array,
                        help='Vector b, in this format: [b0, b1, ... bn]')

    parser.add_argument('m', type=str, choices=['LGMRES', 'BFGS', 'GMRES'],
                        help='Method to solve the inverse problem')

    parser.add_argument('--plot', action="store_true",
                        help='Optionnal flag to plot the results in the end')

    return parser.parse_args()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    # get arguments
    args = parse_args()
    A, b, method, plot = args.A, args.b[0], args.m, args.plot
    x0 = np.zeros(len(b))

    # Setup functor
    S_x = S(A, b)

    # call solver
    x, s, solve_iter = optimize(S_x, method=method, x0=x0)
    logging.info("Optimization finished in " + str(len(solve_iter)-1) + " iteration(s).")
    logging.info("Solution : " + str(x))
    logging.info("Value for solution : " + str(s))

    # display iterations in 3D
    if plot:
        if len(b) != 2:
            logging.warning("Plot only available for 2D problems. Aborting.")
        else:
            display_opt(S_x, (x[0]-3.0, x[0]+3.0), (x[1]-3.0, x[1]+3.0), solve_iter, method)
