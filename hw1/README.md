# Homework 1 : Generalized minimal residual method

## DQS
Folder of the homework 1 by :
* Jonathan Magnin;
* Tim Tuuva.

## Content
* The two files as described by homework 1 : optimizer.py and GMRES.py;
* The requirements, README and gitignore files.

## Dependencies
Run the following command to install required libraries:

`pip install -r requirements.txt`

## How to run
Run the following to solve the given problem using _LGMRES_ and plot the result:

`python optimizer.py A b method --plot`

Where :
- <img src="https://latex.codecogs.com/svg.latex?\Large&space;A \in \mathbb{R}^{n\times n}" title="A statement" />, e.g `"[[8, 1]; [1, 3]]"`

- <img src="https://latex.codecogs.com/svg.latex?\Large&space;b \in \mathbb{R}^{n\times 1}" title="b statement" />, e.g `"[2, 4]"`
- **method**: either `{LGMRES,BFGS,GMRES}`

There is also an optional flag **--plot** for plotting the result.

Make sure to use `;` and not `,` for each new line of the matrix A as an argument of the program!

Finally, the plot is only possible when solving for $x$ in $\mathbb{R}^{2\times 1}$. The **--plot** flag is ignored otherwise.

The command below shows the correct syntax :

`python optimizer.py "[[8, 1]; [1, 3]]" "[2 4]" LGMRES --plot`

For help, type `python optimizer.py -h`

## Expected results
Suppose we have:

<img src="https://latex.codecogs.com/svg.latex?\Large&space;A=\begin{pmatrix}8 & 1\\1 & 3\end{pmatrix}" title="Matrix A statement" />,

<img src="https://latex.codecogs.com/svg.latex?\Large&space;b=\begin{pmatrix}2 \\ 4\end{pmatrix}" title="Vector b statement" />

For each method, we have then the following plots and optimum point:

### BFGS

Running :

`python optimizer.py "[[8, 1]; [1, 3]]" "[2 4]" BFGS --plot`

Yields :

![image info](hw1/doc/BFGS.jpg)

With an optimum point found at: `[0.08695652 1.30434783]`
in 5 iterations

### LGMRES

Running :

`python optimizer.py "[[8, 1]; [1, 3]]" "[2 4]" LGMRES --plot`

Yields :

![image info](hw1/doc/LGMRES.jpg)

With an optimum point found at: `[0.08695652 1.30434783]`
in 2 iterations

### GMRES

Running :

`python optimizer.py "[[8, 1]; [1, 3]]" "[2 4]" GMRES --plot`

Yields :

![image info](hw1/doc/GMRES.jpg)

With an optimum point found at: `[0.08695652 1.30434783]`
in 2 iterations

