# SP4E Autumn 2023
## DQS
This is the SP4E repo of :
* Jonathan Magnin (jonathan.magnin@epfl.ch);
* Tim Tuuva (tim.tuuva@epfl.ch).

## How to
Each homework is in its own folder (like hw1 for homework 1).  
Each homework has its own README with the required content.
