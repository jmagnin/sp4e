"""
Small python plotter for the HW2
Tim Tuuva
Jonathan Magnin
"""

import argparse
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == "__main__":
    # parse
    parser = argparse.ArgumentParser() 
    parser.add_argument("filename", type=str, help="Path of the filename to \
        plot the series results from.")
    args = parser.parse_args()
    
    # read file
    ext = args.filename.split(".")[1]
    sep = None
    if ext == "psv":    # need to specify separator for PSV
        sep = "|"
    df = pd.read_csv(args.filename, sep=sep, engine='python')
    
    # Plot
    plt.figure(figsize=(10,10))
    plt.plot(df['Iteration'], df['Compute'], label='Series computation')
    plt.axhline(y=df['Analytical'][0], color='k', linestyle='--',
        label='Analytical')
    plt.xlabel('Iteration [-]')
    plt.ylabel('Series computation [-]')
    plt.legend()
    plt.show()
