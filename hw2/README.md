# Homework 2

## DQS
Homework 2 folder by :
* Jonathan Magnin;
* Tim Tuuva.

## Folder Description
This folder provides the following filetree :  
```
├── build  
├── CMakeList.txt  
├── doc  
├── README.md  
└── src  
```
It is as described in the homework handout. The only difference is a "doc" folder containing a plantUML file to describe the codes architecture.

## Work distribution
The work is separated in the following steps :

1. Write doc and code skeleton;
2. Implement Series and its children classes;
3. Implement DumperSeries and its children classes (incl Pythin plot script);
4. Implement the main function;
5. Document the results;

Each step is distributed as follow :

* Step 1. by Jonathan;
* Step 2 and 3 in parallel by Jonathan and Tim respectively;
* Step 4 by the first who finishes the previous steps;
* Step 5 Jonathan and Tim together.

As the architecture of the code suggest, steps 2 and 3 are easily parallelizable. The step 1 shall implement dummy child classes for the code to run and allow both tasks 2 and 3 to be executed in parallel.

## Architecture
![](doc/homework_2_architecture.png)

## How to execute

### Compilation of the cpp code
CD into the `hw2` folder and execute the following run :  
```
mkdir build
cd build  
cmake .. 
make   
```

The code should be ready to be executed as `build/src/main`.

### Prepare the python environment
To ensure that the plotter can be used, a virtual environment can be set up.
From the `hw2` folder, run :  
```
python -m venv venv  
source venv/bin/activate  
python -m pip install -r requirements.txt  
deactivate  
```

## How to execute the code
First, activate the python venv (useless if not using the plotter) :
From `hw2`
```
source venv/bin/activate
```

Then, run the code from the `build` folder : 
```
cd build  
./src/main {arithm, pi, integral} N [a b {cube, sin, cos}] {print, write} F P [{"\ ", "\t", "|", ","} filename]
```

The usage is the following :
``` 
Usage:

./build/src/main {arithm, pi, integral} N [a b {cube, sin, cos}] {print, write} F P [{"\ ", "\t", "|", ","} filename]

Where :
        * a,b are numerical integration boundaries;
        * N is the term to evaluate the selected serie on, must be integer;
        * F is the frequency to dump each term of a serie, must be integer;
        * P is number of significant digit to print, must be integer;

[a b {cube, sin, cos}] shall ONLY be provided when using integral option.
```

And is displayed in the terminal if not called accordingly.

The plotter can simply be called with a filename :
```
python plotter.py filename
```

## Examples of executions and results
### Print in terminal examples
From the `build` folder :

Running :
```
./src/main arithm 30 print 3 2
```

Yields :
```
Iteration        Results         Convergence Residual
0       0
3       6
6       21
9       45
12      78
15      1.2e+02
18      1.7e+02
21      2.3e+02
24      3e+02
27      3.8e+02
```

Running :
```
./src/main pi 10000 print 500 10
```

Yields :
```
Iteration        Results         Convergence Residual
0       0       3.141592654
500     3.139684123     0.001908530451
1000    3.140638056     0.0009545973838
1500    3.140956182     0.0006364720861
2000    3.141115272     0.0004773817533
2500    3.141210735     0.0003819186939
3000    3.141274381     0.0003182729625
3500    3.141319844     0.0002728099174
4000    3.141353942     0.0002387116447
4500    3.141380463     0.0002121901799
5000    3.141401681     0.0001909726389
5500    3.141419041     0.0001736125884
6000    3.141433508     0.0001591457119
6500    3.141445749     0.0001469043895
7000    3.141456242     0.0001364117405
7500    3.141465336     0.0001273180465
8000    3.141473293     0.0001193610147
8500    3.141480314     0.000112340066
9000    3.141486554     0.0001060991926
9500    3.141492138     0.0001005152291
```


Running :
```
./src/main integral 300 0 3 cube print 30 3
```

Yields :
```
a= 0 b= 3func = cube
Iteration        Results         Convergence Residual
0       0
30      21.6
60      20.9
90      20.7
120     20.6
150     20.5
180     20.5
210     20.4
240     20.4
270     20.4
```


### Plot examples
From the `build` folder :

Running :
```
./src/main arithm 30 write 3 5
python ../plotter.py out_default.txt
```

Yields :  

![](doc/arithm_plot.png)

Running :
```
./src/main pi 100 write 3 10 "\t" pi_file
python ../plotter.py pi_file.txt
```

Yields :  

![](doc/pi_plot.png)


Running :
```
./src/main integral 100 0 1 cube write 3 10 "," cube_file
python ../plotter.py cube_file.csv 
```

Yields :  

![](doc/cube_plot.png)

Running :
```
./src/main integral 1000 0 3.1415 cos write 3 10 "|" cos_file
python ../plotter.py cos_file.psv
```

Yields :  

![](doc/cos_plot.png)


### Complexity of the Series (Exercice 5)

#### 1. GLobal complexity
Before the modification, the program can be described awith the following pseudo
code :
```
sum <- 0
for i in [1:F:N]
    for k in [1:i]
        sum <- S(k)
    end for
end for
return sum
```

Which yields a complexity of :

$$
1 + F + 2F + 3F + ... + \text{floor}(\frac{N}{F})F \; = \;
F \cdot (\sum\limits_{n=1}^{\text{floor}(\frac{N}{F})} n) \; \approx \;
\frac{N^2}{F}
$$

#### 4. New complexity
By keeping in memory the current sum and index, thw pseudo code becomes :

```
sum <- 0
for i in [1:F:N]
    for k in [i-F:i]
        sum <- S(k)
    end for
end for
return sum
```

Which yields a complexity of :

$$
1 + (F-1) + (2F-F) + (3F-2F) + ... + (\text{floor}(\frac{N}{F})F -
(\text{floor}(\frac{N}{F})-1)F) \; = \; 
F \cdot \text{floor}(\frac{N}{F}) \; \approx \;
N
$$

which is much better !

#### 5. For pi, when computing from N to 1
This does not change the complexity of $N$. Indeed, it computes backward from 
`i` to `i-F`, but the same amount of partial sums have to be computed in the
end.

## Additional remarks
None.

