#include "write_series.hh"
#include "series.hh"
#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <stdexcept>
#include <iomanip>

// WriteSeries constructor
WriteSeries::WriteSeries(Series &series, const unsigned int &max_iter,
                        const unsigned int &frequency,
                        const unsigned int &precision)
                        : DumperSeries(series, max_iter, frequency, precision),
                        file_sep("\\"), file_ext(".txt"){
}

// Overridden dump functions for the WriteSeries
void WriteSeries::dump(std::ostream & os){
  os << std::setprecision(this->precision);
  
  os << "Iteration" << this->file_sep;
  os << "Compute" << this->file_sep;
  os << "Analytical" << std::endl;

  double convergence = this->series->getAnalyticPrediction();

  for(unsigned int i=0; i < this->max_iter; i=i+1) {
    os << i << this->file_sep << std::flush;
    os << this->series->compute(i) << this->file_sep << std::flush;
    os << convergence << std::endl;
  }
}

// Select the appropriate file extension and store the file separator to be used
void WriteSeries::setSeparator(const std::string &sep) {
  this->file_sep = sep;
  if(this->file_sep == "\\") {
    this->file_sep = " ";
    this->file_ext = ".txt";
  }

  else if(this->file_sep == "\\t"){
    this->file_sep = "\t";
    this->file_ext = ".txt";
  }
  else if(this->file_sep == ",") {
    this->file_ext = ".csv";
  }
  else if(this->file_sep == "|") {
    this->file_ext = ".psv";
  }
  else {
    throw std::invalid_argument( "Incompatible separator "
                                  + this->file_sep + " ." );
  }
}
