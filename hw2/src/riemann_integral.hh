#ifndef RIEMANN_INTEGRAL_HH
#define RIEMANN_INTEGRAL_HH

#include "series.hh"

/**
 * Define the type of function pointer to integrate univariate real
 * function.
 */
typedef double (*realUnivariateFunc)(double x);

class RiemannIntegral : public Series{
    public:
        /**
         * @brief Construct a new Riemann Integral object
         * 
         * @param a lower integration bound
         * @param b upper integration bound
         * @param f function to integrate (real univariate)
         */
        RiemannIntegral(double a, double b, realUnivariateFunc f);

        /**
         * @brief Estimate the integral using a Riemann serie in term N.
         * 
         * @param N term of the Riemann serie
         * @return double, Riemann integral value for N terms
         */
        double compute(unsigned int N) override;

    protected :
        double a;
        double b;
        realUnivariateFunc f;
};

#endif