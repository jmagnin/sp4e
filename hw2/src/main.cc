#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <memory>
#include <cmath>
#include "series.hh"
#include "compute_arithmetic.hh"
#include "compute_pi.hh"
#include "riemann_integral.hh"
#include "print_series.hh"
#include "write_series.hh"
#include <iomanip>

/**
 * @brief Prints the help fo this program in the temrinal
 * 
 */
void printUsage(std::string name){
  std::cout << "Usage:";
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << name;
  std::cout << " {arithm, pi, integral}";
  std::cout << " N"; 
  std::cout << " [a b {cube, sin, cos}]"; 
  std::cout << " {print, write}";                 
  std::cout << " F P";                
  std::cout << " [{\"\\ \", \"\\t\", \"|\", \",\"} filename]"; 
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "Where :";
  std::cout << std::endl;
  std::cout << "\t* a,b are numerical integration boundaries;";
  std::cout << std::endl;
  std::cout << "\t* N is the term to evaluate the selected serie on, must be" 
                " integer;";
  std::cout << std::endl;
  std::cout << "\t* F is the frequency to dump each term of a serie, must be"
                " integer;";
  std::cout << std::endl;
  std::cout << "\t* P is number of significant digit to print, must be"
                " integer;";
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << "[a b {cube, sin, cos}] shall ONLY be provided when using"
                " integral option.";
  std::cout << std::endl;
  std::cout << std::endl;
}

/**
 * @brief Available real univariate functions to integrate
 * 
 * @param x, input
 * @return double, output 
 */
inline double cube(double x){return std::pow(x, 3.0);};
inline double cosine(double x){return std::cos(x);};
inline double sine(double x){return std::sin(x);};


/**
 * @brief main function of the homework 2
 * 
 * @param argc 
 * @param argv 
 * @return int 
 */
int main(int argc, char ** argv){

  // Args parsing
  if(argc < 4) {
    printUsage(argv[0]);
    std::exit(EXIT_FAILURE);
  }

  // Concatenating
  unsigned int argnow = 1;
  std::stringstream args;
  std::string output_type, series_type;
  std::string sep_type = "\\";
  std::string out_filename = "out_default";
  unsigned int n_terms = 0;
  unsigned int frequency = 1;
  unsigned int precision = 2;
  double a = 0.0;
  double b = 0.0;
  std::string func = "";
  try{
    // get serie type and N iter
    args << argv[argnow++] << " " << argv[argnow++] << " ";
    args >> series_type >> n_terms ;

    // get integration boundaries if required
    if(series_type == "integral"){
      if(argc < 9){
        printUsage(argv[0]);
        std::exit(EXIT_FAILURE);
      }
      args << argv[argnow++] << " " << argv[argnow++] << " " << argv[argnow++]
        << " ";
      args >> a >> b >> func;
      std::cout << "a= " << a << " b= " << b << "func = " << func << std::endl;
    }else if(argc > 8){
      printUsage(argv[0]);
      std::exit(EXIT_FAILURE);
    }

    // get dumper, dumping frequency and precision
    if(argc < argnow + 3){
      printUsage(argv[0]);
      exit(EXIT_FAILURE);
    }
    args << argv[argnow++] << " " << argv[argnow++] << " " << argv[argnow++]
      << " ";
    args >> output_type >> frequency >> precision;
    
    // get separator and filename if present
    if(argc == argnow + 1){
      printUsage(argv[0]);
      exit(EXIT_FAILURE);
    }
    else if((argc) > argnow+1){
      args << argv[argnow++] << " " << argv[argnow++] << " ";
      args >> sep_type >> out_filename;
    }
  }
  catch(std::exception &e){
    printUsage(argv[0]);
    std::exit(EXIT_FAILURE);
  }


  // Creation of the Serie
  std::shared_ptr<Series> serie;
  if(series_type == "arithm") {
    serie = std::make_shared<ComputeArithmetic>();    
  }
  else if(series_type == "pi") {
    serie = std::make_shared<ComputePi>();    
  }
  else if(series_type == "integral") {
    if(func == "cube"){
      serie = std::make_shared<RiemannIntegral>(a, b, cube); 
    } else if(func == "cos"){
      serie = std::make_shared<RiemannIntegral>(a, b, cosine); 
    }else if(func == "sin"){
      serie = std::make_shared<RiemannIntegral>(a, b, sine); 
    } else{
      throw std::invalid_argument("Invalid option for the integral function!"
                                  "Choose between: {cube, cos or sin}" );
    }
  }
  else {
    throw std::invalid_argument("Invalid option for the output style! Choose"
                                "between: {arithm, pi or integral}" );
  }
  
  // Creation of the DumperSeries
  if(output_type == "print") {
    PrintSeries dumper = PrintSeries(*serie, n_terms, frequency, precision);
    dumper.setPrecision(precision);
    std::cout << dumper << std::flush;
  }
  else if(output_type == "write") {
    WriteSeries dumper = WriteSeries(*serie, n_terms, frequency, precision);
    dumper.setSeparator(sep_type);

    std::ofstream myfile;
    myfile.open(out_filename + dumper.getFileExt());
    myfile << dumper << std::flush;
    myfile.close();
  }
  else {
    throw std::invalid_argument("Invalid option for the output style! Choose"
                                "between: {print or write}" );
  }

  return EXIT_SUCCESS;
}
