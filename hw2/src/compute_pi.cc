#include <cmath>
#include <numbers>
#include "compute_pi.hh"

/**
 * 
 */
double ComputePi::compute(unsigned int N){
    // init
    this->initSerie(N);

    // compute reversly tokep precision
    double sum = this->current_value;
    for(int i=N; i>=this->current_index; i--){
        sum += (std::pow((double)i, -2.0));
    }

    this->current_index = N+1;
    this->current_value = sum;

    return std::sqrt(6.0*sum);
}

/**
 * 
 */
double ComputePi::getAnalyticPrediction(){
    return std::numbers::pi;
}