#ifndef DUMPER_SERIES_HH
#define DUMPER_SERIES_HH

#include <cmath>
#include "series.hh"
#include <string>


class DumperSeries{
    public:
        /**
         * @brief Construct a new DumperSeries object
         *
         * @param Series object,
         * @param max_iter number of iteration,
         * @param frequency of the iteration,
         * @param precision of the output
         */
        DumperSeries(Series &series, const unsigned int &max_iter,
                        const unsigned int &frequency=1,
                        const unsigned int &precision=2)
                        : series(&series), max_iter(max_iter),
                        frequency(frequency), precision(precision){}

        /**
         * @brief Iterate and compute N terms of the Series and dump the
         * results either in a file or to console
         *
         * @param output stream
         * @return void
         */
        virtual void dump(std::ostream &os) = 0;

        /**
         * @brief Set the number of significant number to print when dumping
         *
         * @param Precision to achieve
         * @return void
         */
        void setPrecision(const unsigned int &precision){
            this->precision = precision;
        };


    protected:
        Series *series;
        unsigned int precision;
        const unsigned int max_iter;
        const unsigned int frequency;
};

/**
 * @brief << operator overloading to print with ease the DumperSeries object
 *
 * @param stream to be written to, the DumperSeries to dump into the stream
 * @return std::ostream, containing the dumped output of the DumperSeries
 */
inline std::ostream & operator <<(std::ostream & stream, DumperSeries & _this) {
    _this.dump(stream);
    return stream;
}

#endif
