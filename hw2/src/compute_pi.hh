#ifndef COMPUTE_PI_HH
#define COMPUTE_PI_HH

#include "series.hh"

class ComputePi : public Series{
    public:
        /**
         * @brief Compute Pi serie in Nth term
         * 
         * @param N term
         * @return double, value of thePi serie in Nth term 
         */
        double compute(unsigned int N) override;

        /**
         * @brief Get the Analytic Prediction of the Pi serie, which is Pi.
         * 
         * @return double, Pi
         */
        double getAnalyticPrediction() override;
};

#endif