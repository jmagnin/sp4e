#ifndef PRINT_SERIES_HH
#define PRINT_SERIES_HH

#include "series.hh"
#include "dumper_series.hh"
#include <iostream>

class PrintSeries : public DumperSeries{
    public:
        /**
        * @brief Construct a new PrintSeries object
        * 
        */      
        PrintSeries(Series &series, const unsigned int &max_iter,
                    const unsigned int &frequency=1,
                    const unsigned int &precision=2);

        /**
         * @brief Iterate and compute N terms of the Series and dump the
         * results either in a file or to console
         *
         * @param output stream
         * @return void
         */
        void dump(std::ostream &os = std::cout) override;
};

#endif
