#include "compute_arithmetic.hh"


/**
 * 
 */
double ComputeArithmetic::compute(unsigned int N){
    // init
    this->initSerie(N);

    // compute
    double sum = this->current_value;
    for(int i=this->current_index; i<=N; i++){
        sum += ((double)i);
    }

    this->current_index = N+1;
    this->current_value = sum;


    return sum;
}