#ifndef SERIES_HH
#define SERIES_HH

#include <cmath>

class Series{
    public:
        /**
        * @brief Construct a new Series object
        * 
        */
        Series() : current_index(1), current_value(0.0){}

        /**
         * @brief Computes the value of the serie in Nth term
         * 
         * @param N term to compute
         * @return double, value in term N
         */
        virtual double compute(unsigned int N) = 0;

        /**
        * @brief Get the Analytic Prediction of the serie.
        * 
        * @return double, analytical predition if it exists, NaN otherwise.
        */
        virtual double getAnalyticPrediction(){return std::nan("1");}

    protected:
        unsigned int current_index;
        double current_value;

        /**
         * @brief initialize serie computation by choosing to start from current
         * index or to restart.
         *  
         * @param N, index of the serie to compute
         * 
         */
        void initSerie(unsigned int N){
            if (N > this->current_index){
                return;
            }else{
                this->current_index = 1;
                this->current_value = 0.0;
            }
        }

};

#endif
