#include "print_series.hh"
#include "series.hh"
#include <iostream>
#include <cmath>
#include <iomanip>

// PrintSeries constructor
PrintSeries::PrintSeries(Series &series, const unsigned int &max_iter,
                        const unsigned int &frequency, const unsigned int
                        &precision)
                        : DumperSeries(series, max_iter, frequency, precision){
}

// Overridden dump for the PrinterSeries
void PrintSeries::dump(std::ostream & os){

  double convergence = this->series->getAnalyticPrediction();

  os << std::setprecision(this->precision);
  os << "Iteration\t Results\t Convergence Residual" << std::endl;

  // At each iteration, we compute the ith term of the series and
  // compute the residual convergence if it is not nan
  for(unsigned int i=0; i < this->max_iter; i += this->frequency) {
    double result = this->series->compute(i);
    os << i <<  "\t" << result;
    
    if(!std::isnan(convergence)) {
      os << "\t" << convergence - result;
    }
    os << std::endl;
  }
}
