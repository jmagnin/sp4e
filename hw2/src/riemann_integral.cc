#include <exception>
#include "riemann_integral.hh"

/**
 * 
 */
 RiemannIntegral::RiemannIntegral(double a, double b, realUnivariateFunc f) 
                                : Series() {
    this->a = a;
    this->b = b;
    this->f = f;
 }

/**
 *
 */
double RiemannIntegral::compute(unsigned int N){
    double sum = 0.0;
    double dx = (this->b - this->a) / ((double)N);
    double xi = this->a;
    for(int i=1; i<=N; i++){
        xi += dx;
        sum += (this->f(xi) * dx);
    }

    return sum;
}