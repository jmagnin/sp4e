#ifndef WRITE_SERIES_HH
#define WRITE_SERIES_HH

#include "dumper_series.hh"
#include "series.hh"
#include <string>


class WriteSeries : public DumperSeries{
    public:
        WriteSeries(Series &series, const unsigned int &max_iter,
        const unsigned int &frequency=1, const unsigned int &precision=2);

        /**
         * @brief Iterate and compute N terms of the Series and dump the results
         * either in a file or to console
         *
         * @param output stream
         * @return void
         */
        void dump(std::ostream & os) override;

        /**
         * @brief Set the separator char when dumping
         *
         * @param The separator to be used  (" ", "\t", "," or "|")
         * @return void
         */
        void setSeparator(const std::string &sep);

        /**
         * @brief Get the file extension when writing in a file 
         *
         * @return the file extension to be used (".txt, ".csv" or ".psv")
         */
        std::string getFileExt(void) const { return file_ext; }

    protected:
        std::string file_sep;
        std::string file_ext;
};

#endif
