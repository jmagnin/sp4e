#ifndef COMPUTE_ARITHMETIC_HH
#define COMPUTE_ARITHMETIC_HH

#include "series.hh"

class ComputeArithmetic : public Series{
    public:
        /**
         * @brief Computes the arithmetic serie.
         * 
         * @param N term of the serie
         * @return double, value of the arithmetic serie in term N
         */
        double compute(unsigned int N) override;
};

#endif