from dumper_series import DumperSeries
from series import Series
import matplotlib.pyplot as plt


class PlotSeries(DumperSeries):
    """
    DumperSeries to plot the series in matplotlib
    """

    def __init__(self, series: Series, f: int, maxiter: int):
        """
        Constructor
        """
        super().__init__(series, f, maxiter)

    def dump(self):
        """
        Plot the series in matplotlib (blocks the execution !)
        :return:
        """
        super().dump()
        # compute series values
        vals = [self.series.compute(n) for n in self._DumperSeries__terms]

        # get analytical prediction if it exist
        try:
            anal = self.series.get_analytic_prediction()
        except Exception:
            anal = None

        # plot result
        ax = plt.figure().add_subplot()
        ax.plot(self._DumperSeries__terms, vals, label="serie")
        if anal is not None:
            xmin = self._DumperSeries__terms[0]
            xmax = self._DumperSeries__terms[-1]
            ax.hlines(anal, xmin, xmax, color="black", linestyles="--", label="analytical predict")
        ax.set_xlabel("N")
        ax.set_ylabel("Value")
        plt.grid()
        plt.legend()
        plt.show()



