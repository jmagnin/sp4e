"""
Main file of the Serie 5
jmagnin
"""
from series import Series
from compute_arithmetic import ComputeArithmetic
from compute_pi import ComputePi
import argparse
from print_series import PrintSeries
from plot_series import PlotSeries

# parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("serie", help="Either pi either arithm")
parser.add_argument("-N", "--term", type=int, help="Serie term to compute (integer)")
parser.add_argument("-f", "--freq", type=int, help="Frequency at which to dump the serie (integer)")
parser.add_argument("--plot", action="store_true", help="Plot serie if present, otherwise print in terminal")
args = parser.parse_args()
print("Computing ", args.serie, " serie up to term ", args.term)

# compute desired serie
result = 0
if args.serie == "arithm":
    s = ComputeArithmetic()
elif args.serie == "pi":
    s = ComputePi()
else :
    parser.print_help()
    exit()

# dump serie
maxiter = int(args.term / args.freq)
if args.plot:
    PlotSeries(s, args.freq , maxiter).dump()
else:
    PrintSeries(s, args.freq , maxiter).dump()