from series import Series
import numpy as np


class ComputeArithmetic(Series):
    """
    Child class of Series implementing  arithmetic series :

        $S_n = \sum_1^N k$
    """

    def __init__(self):
        """
        Constructor
        """
        super().__init__()

    def compute(self, N):
        """
        Compute an arithmetic serie from 1 to N.
        :param N: the serie term
        :return: the value of the serie in N
        """
        serie_value = np.sum(np.array(range(1,N+1)))
        return serie_value

    def get_analytic_prediction(self):
        """
        No analytical prediction for this serie
        :return:
        """
        raise Exception("No analytical prediction")
