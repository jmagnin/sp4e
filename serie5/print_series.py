from dumper_series import DumperSeries
from series import Series

class PrintSeries(DumperSeries):
    """
    DumperSeries to print the series in the terminal
    """
    def __init__(self, series: Series, f: int, maxiter: int):
        """
        Constructor
        """
        super().__init__(series, f, maxiter)
        
    def dump(self):
        """
        Print the series in the temrinal
        :return:
        """
        super().dump()
        # cmpute series values
        vals = [self.series.compute(n) for n in self._DumperSeries__terms]
        dumpstr = str(vals)

        # get analytical prediction if it exist
        try:
            anal = self.series.get_analytic_prediction()
            msg = "This serie converges towards : " + str(anal)
        except Exception:
            msg = "This serie does not converges"

        # print result
        print(msg)
        print("Dumped serie : ", dumpstr)



