from series import Series
import numpy as np
import math


class ComputePi(Series):
    """
    Child class of Series implementing  Pi serie :

        $S_n = \sqrt{6*\sum_1^N k^{-2}}

    This serie converges towards Pi
    """

    def __init__(self):
        """
        Constructor
        """
        super().__init__()

    def compute(self, N):
        """
        Compute the Pi serie up to N-th term
        :param N: the serie term
        :return: the value of the serie in N
        """
        inner_sum = np.sum(np.array([1.0/(k**2) for k in range(1,N+1)]))
        serie_value = math.sqrt(6.0*inner_sum)
        return serie_value

    def get_analytic_prediction(self):
        """
        its pi :)
        :return: Pi
        """
        return np.pi
