from abc import ABC, abstractmethod
from series import Series
import numpy as np

class DumperSeries(ABC):
    """
    Interface to dump the content of a Series
    """

    def __init__(self, series: Series, f: int, maxiter: int):
        """
        Constructor
        :param series: Serie onject to dump
        :param f: dumping frequency
        :param maxiter: max iterations to dump at frequency f
        """
        self.series = series
        self.f = f
        self.maxiter = maxiter

        # private arguments
        self.__terms = None

    @abstractmethod
    def dump(self):
        """
        Dump the serie with the parameters in construction
        :return:
        """
        # compute term vector
        stop = self.maxiter * self.f
        self.__terms = np.arange(1, stop, self.f)
