"""
Interface describing a Serie
"""
from abc import ABC, abstractmethod

class Series(ABC):
    """
    Abstract ABC class (i.e. interface) for mathematical series.
    """
    def __init__(self):
        """
        Constructor<
        """
        pass

    @abstractmethod
    def compute(self, N):
        """
        Compute the value of the serie for the given term
        :param N: term of the serie
        :return: value of the serie at N
        """
        raise Exception("Pure virtual method")

    @abstractmethod
    def get_analytic_prediction(self):
        """
        return the analytic prediction of the serie
        :return: analytic prediction
        """
        raise Exception("pure virtual function")