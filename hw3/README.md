# HW3

## Ex1. Code Exploration
*   The PArticles are child classes of the Particle interface. So is the MaterialPoint.
    It atores the current state of the Particle and can be updated. The MaterialPoint
    specifically stores temperature and heatflow;
*   Like each Particle child class, the MaterialPoint has its own factory implementing the
    ParticleFactory interface;
*   In order to compute values relative to meterial points (i..e the temperature), a class
    implementing the Compute interface is made : the ComputeTemperature class. 
    *   This class needs to compute the next state of all the particles as a whole as it uses
        the DFT to solve the heat equation;
    *   All the Particles positions are converted in a grid which is represented with a Matrix object
        ( which implicitely is a squared matrix);
    *   The matrix is then given to the transform and itransform methods of the FFT class, which wraps
        the FFTW library in order to compute forward and backward FFT with complex numbers;
    *   Finally, the next step is computed (by integrating over time) and the MaterialPoint temperature
        is updated;
*   With all of this, a simulation of the propagation of heat through a 2D grid of material points
    can be simulated by using the MaterialPoint class just as any other particle, with a System and its
    Evolution computing the state of the MaterialPoints step by step and dumping the results in CSV files
    as the MaterialPoint overrides the `<<` operator.

## Ex2. Link to FFTW
The FFTW library cna be linke dor not when issuing the `cmake` command, the next sections describe
how to do it in detail. Unit tests are available in `test_fft.cc`.

## Ex3. Interface implementation
The interface to FFTW is implemented in the `fft.hh` and `fft.cc` files.

## Ex4. Solver implementation
The solver is implemented in `compute_temperature.hh` and `compute_temperature.cc`. It uses the 
`Matrix` and `FFT` classes to do so.Unit tests that test the equilibrium cases depicted in 
points **4.2, 4.3 and 4.4** are available in `test_heat.cc`. The point **4.5** is not treated.
Finally, the point **4.6** uses a python script : `generate_materialpoints.py` from which there is
already an example output as `heat_expl.csv` that can be visualized with paraview by following the
steps given later in this README.

## Work repartition and state

### Phase 1 : Implement FFT + its tests (Ex 3) and implement ComputeTemperature (Ex 4.1)

- [x] Jonathan : implement FFT;
- [x] Jonathan : implement unit test for FFT;
- [x] Tim : Implement ComputeTemperature.

### Phase 2 : Test ComputeTemperature

- [x] Tim : implement test for homogenous temperature (Ex 4.2);
- [x] Jonathan : implement test for sin volumetric heatsource (Ex 4.3);
- [x] Tim : OPTIONAL : implement test for +/- 1 volumetric heatsource (Ex 4.4);
- [ ] Tim : OPTIONAL : python script to generate circular heatsource (Ex 4.5);
- [x] Jonathan : Generate and document paraview for 512x512 particles.

### Phase 3 : hand in

- [x] Jo+Tim : review and refactor code;
- [x] Jonathan : finalize doc in html;
- [x] Jonathan : finish README;
- [x] Jo+Tim : final double check against handout in fresh cloned repo.

## How to build

### Requirements

*   The **fftw3** library is required. To install it on your system,
see [the fftw webpage](http://fftw.org/);
*   Optional : Python 3.

### Get and prepare the directory

Upon pulling, one should have the following directory in `hw3` :

``` bash
.
├── example_dumps
├── fig
├── README.md
└── src
```

Execute the following commands to create the build and dumps folders :

``` bash
mkdir build dumps
tree -L 1
```

The output should be :

``` bash
.
├── build
├── dumps
├── example_dumps
├── README.md
└── src
```

Then, populate the googletest submodule by typing :

``` bash
cd src
git submodule init googletest
git submodule update googletest
cd ..
```

### Build the documentation

To build the documentation, run the following commands from `hw3` :

``` bash
cmake src/ -B build/ -DUSE_FFT=1
make -C build/ doc
```

The `cmake` command can be omitted if it was already executed
previously for the executables.  

The documentation is generated in the `build/html/` folder and can
be read by opening `build/html/index.html` in a web browser.  

### Build the code

The code can now be compiled and linked.  
When using cmake, the fftw3 library, which is required, can be
linked by adding the flag `-DUSE_FFT=1`. To build the code, issue the
following commands from `hw3` :

``` bash
cmake src/ -B build/ -DUSE_FFT=1
make -C build/
```

The `cmake` command can be omitted if it was already executed
previously for the documentation.  

The code should build successfully and the `hw3/build/`
directory shall now contain these 4 executables (among other
files and folders) :  

``` bash
├── particles
├── test_fft
├── test_heat
└── test_kepler
```

## How to use

### Run the tests

To ensure that the code executes properly, tests are avalable.
One can simply run the test by calling the executable :

``` bash
./build/test_fft
./build/test_heat
```

These tests shall be successful and end with a `[  PASSED  ]` statement.

### Generate a csv initialization file

Before executing the program, one need an initlalization file containing
the particles and their initial state.

An example case exists in `src/heat_expl.csv`. One can tweak and regenerate
it using the python script `src/generate_materialpoints.py`.
This script uses basic python functionalities (numpy). Any normal, recent
python installation should suffice. To use it, simply tun :

```bash
cd src/
python generate_materialpoints.py
cd ..
```

The user may also generate any initialization file respecting the order : 

```bash
X Y Z VX VY VZ FX FY FZ mass temperature heatrate
```

In each line, *Space separated !*

### Run the program

The program should be used as follow :

``` bash
Usage: ./build/particles nsteps dump_freq input.csv particle_type timestep
        particle type can be: planet, ping_pong, material_point
```

As an example, one may run :

``` bash
./build/particles 170 17 src/heat_expl.csv material_point 0.001
```

The program will dump the state of the particles accordingly in the `dumps/` folder, which can then be used to visualize the result using the Paraview
software.

### Visualize temperature on Paraview

The dumped csv files can be ued to visualize the result with Paraview.
Example dump is present in the `example_dumps/` folder. To see the result,
execute the following steps on Paraview :

1. Load the `step-XXXXX.csv` files from `dumps/` or `example_dumps/` in one block;
2. In the properties :
    * Untick the box "Have header";
    * Set the separator as a space character;
    * click on "Apply";
    * You should now see the content of the files in a spreadsheet view;
3. Add a "Table to Points" filter to the loaded files;
4. In the properties of the "Table to Points" :
    * Set X column as "Field 0";
    * Set Y column as "Field 1";
    * Set Z column as "Field 2";
5. Click on apply;
6. Click on the little eye next to the "TabletoPoints1" in
    the pipeline browser to open it. You should now see
    a white square on the render area. If it doesn't show,
    click on the render area;
7. Again in the "Table to Points 1" porperties :
    * Under "Display", set "Representation" to "Points";
    * Under "Coloring", select the "Field 13" and click on the small buton
        "Rescale to data range over all timesteps" (it may take a lot of time if there are many dumps);
8. Click on the "Play" button;
9. The heat will now evolve according to the heat equation.

### Example of visualization

If performing the steps given above with the example
in `example_dumps/`, the user should see :

First step :  
![First step](./fig/step_0.png)  

Last step :  
![Last step](./fig/step_9.png)

## Comments

### Unstability and parameters

We have remarked that the solving of the heat equation is quite unstable.
Large time step, small heat capacity or large conductivity may cause
the state of the simulation to diverge and become chaotic.
