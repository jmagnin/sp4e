#include "matrix.hh"
#include "my_types.hh"
#include "fft.hh"
#include <complex.h>
#include <fftw3.h>

/* ------------------------------------------------------ */

Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
    // create result matrix
    UInt size = m_in.size();
    Matrix<complex> res = Matrix<complex>(size);

    // compute FFT (fftw_complex natively compatible with std::complex)
    fftw_plan p = fftw_plan_dft_2d(size, size,
                        (fftw_complex*)m_in.data(), (fftw_complex*)res.data(),
                        FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);

    // return
    return res;
}

/* ------------------------------------------------------ */

Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
    // create result matrix
    UInt size = m_in.size();
    Matrix<complex> res = Matrix<complex>(size);

    // compute FFT (fftw_complex natively compatible with std::complex)
    fftw_plan p = fftw_plan_dft_2d(size, size,
                        (fftw_complex*)m_in.data(), (fftw_complex*)res.data(),
                        FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);

    // normalize
    res /= (size * size);

    // return
    return res;
}

/* ------------------------------------------------------ */

Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
    // construct matrix
    Matrix<std::complex<int>> frequ = Matrix<std::complex<int>>(size);
    
    // fill wave numbers
    for (auto&& entry : index(frequ)) {
        int i = std::get<0>(entry);
        int j = std::get<1>(entry);
        auto& val = std::get<2>(entry);

        if(i < size/2){
            val.real(i);
        } else{
            val.real(i-size);
        }

        if(j < size/2){
            val.imag(j);
        } else{
            val.imag(j-size);
        }
    }

    // return
    return frequ;
}
