#include "compute_temperature.hh"
#include "csv_reader.hh"
#include "csv_writer.hh"
#include "material_point.hh"
#include "material_points_factory.hh"
#include "system.hh"
#include <gtest/gtest.h>
#include <cmath>
#include <math.h>

/*****************************************************************/

/**
 * @brief Fixture interface to set up a heat equation problem.
 * Initilaizes the particles with their respective temperature
 * and heat source and prepares a system to execute one small
 * stime step.
*/
class HeatTestCase : public ::testing::Test {
protected:
  /**
  * @brief General SetUp for all HeatTestCase inherited unit tests
  * create and set all material points on a grid and call polymorphed
  * initialization functions for the heat and temperature distributions.
  * 
  * @param void
  * @return void
  */
  void SetUp() override {
    MaterialPointsFactory::getInstance();
    std::vector<MaterialPoint> points;
    int n_row = std::sqrt(n_points);

    for (UInt i = 0; i < n_row; ++i) {
      for (UInt j = 0; j < n_row; ++j) {
        MaterialPoint m;
        
        m.getPosition()[0] = i;
        m.getPosition()[1] = j;
        m.getPosition()[2] = 0.0;

        m.getMass() = 1.;
        m.getTemperature() = this->initialCondition(i, j);
        m.getHeatRate() = this->heatRate(i, j);
        points.push_back(m);
      }
    }

    for (auto& m : points) {
      system.addParticle(std::make_shared<MaterialPoint>(m));
    }

    heat = std::make_shared<ComputeTemperature>(1.0, 1.0, 1E-3);
  }

  /**
   * @brief Computes the heatrate for the test case
  */
  virtual Real heatRate(Real x, Real y) = 0;

  /**
   * @brief Computes the initial conditions for the test case.
  */
  virtual Real initialCondition(Real x, Real y) = 0;

  /**
   * @brief Computes the expected temperature for the test case
  */
  virtual Real expectedTemp(Real x, Real y) = 0;

  System system;
  UInt n_points = 256;
  std::shared_ptr<ComputeTemperature> heat;
};

/**
 * @brief Fixture class to setup a homogenous heat problem :
 *  * No heatsource;
 *  * homogenous temperature for all particles.
 * This is a stationary scenario : the initial state shall
 * not change.
*/
class Homogenous : public HeatTestCase {
protected:
  /**
   * @brief Computes the heatrate for the homogenous case
  */
  Real heatRate(Real x, Real y) override {
    return 0.0;
  }

  /**
   * @brief Computes the initial conditions for the homogenous test case.
  */
  Real initialCondition(Real x, Real y) override {
    return 20.0;
  }

  /**
   * @brief Computes the expected temperature for the homogenous test case
  */
  Real expectedTemp(Real x, Real y) override {
    return 20.0;
  }
};

/**
 * @brief Fixture class to setup a sinus in x heat problem.
 * The heatsource and the initial condition match perfectly in
 * a stationary state (hot-cold-hot-cold...) and shall therefore
 * not change.
*/
class Sinus : public HeatTestCase {
protected:
  Real L = std::sqrt(n_points);

  /**
   * @brief Computes the heatrate for the homogenous case
  */
  Real heatRate(Real x, Real y) override {
    Real n_row = std::sqrt((Real)n_points);
    Real xx = ((Real)x)*2.0/(n_row-1) - 1.0;
    return std::pow(2*M_PI/L, 2.0) * std::sin(2*M_PI*xx / L);
  }

  /**
   * @brief Computes the initial conditions for the homogenous test case.
  */
  Real initialCondition(Real x, Real y) override {
    Real n_row = std::sqrt((Real)n_points);
    Real xx = ((Real)x)*2.0/(n_row-1) - 1.0;
    return std::sin(2*M_PI*xx / L);
  }

  /**
   * @brief Computes the expected temperature for the homogenous test case
  */
  Real expectedTemp(Real x, Real y) override {
    return initialCondition(x, y);
  }
};


/**
 * @brief Fixture class to setup 2 spikes in x heat problem.
 * The heatsource is distributed with 2 spikes at x = +-1/2
 * The equilibrium temperature should look like triangular shaped distribution:
 * (-x-1, x, -x+1) for (x<-1/2, -1/2 < x < 1/2, x > 1/2)
 */
class Triangle : public HeatTestCase {
protected:
  Real L = std::sqrt(n_points);

  /**
   * @brief Computes the heatrate for the homogenous case
  */
  Real heatRate(Real x, Real y) override {
    Real n_row = std::sqrt((Real)n_points);
    Real xx = ((Real)x)*2.0/(n_row-1) - 1.0;
    if(xx == 0.5)
      return 1.0;
    else if(xx == -0.5)
      return -1.0;
    else
      return 0.0;
  }

  /**
   * @brief Computes the initial conditions for the homogenous test case.
  */
  Real initialCondition(Real x, Real y) override {
    Real n_row = std::sqrt((Real)n_points);
    Real xx = ((Real)x)*2.0/(n_row-1) - 1.0;
    if(xx > 0.5)
      return -xx + 1.0;
    else if(xx <= -0.5)
      return -xx - 1.0;
    else
      return xx;
  }

  /**
   * @brief Computes the expected temperature for the homogenous test case
  */
  Real expectedTemp(Real x, Real y) override {
    return initialCondition(x, y);
  }
};


/*****************************************************************/
/**
 * @brief Unit test based on the { @see Homogenous } fixture
 * to test that the homogenous case is stationary.
 */
TEST_F(Homogenous, compute_temperature) {
  
  // Executing one step of the simulation
  heat->compute(system);
  CsvWriter writer("tmp_file");
  writer.compute(system);

  // test compared to closed form 
  for(auto par = system.begin(); par < system.end(); ++par) {
    UInt i = par->get()->getPosition()[0];
    UInt j = par->get()->getPosition()[1];
    ASSERT_NEAR(static_cast<MaterialPoint&>(*par).getTemperature(), expectedTemp(i, j), 1e-4);
  }
  
}

/*****************************************************************/
/**
 * @brief Unit test based on the { @see Sinus } fixture
 * to test that a sinusoidal heatsource in X with the corresponding
 * initial temperature remains in equilibrium.
 */
TEST_F(Sinus, compute_temperature) {
  
  // Executing one step of the simulation
  heat->compute(system);
  CsvWriter writer("tmp_file");
  writer.compute(system);

  // test compared to closed form 
  for(auto par = system.begin(); par < system.end(); ++par) {
    UInt i = par->get()->getPosition()[0];
    UInt j = par->get()->getPosition()[1];
    ASSERT_NEAR(static_cast<MaterialPoint&>(*par).getTemperature(), expectedTemp(i, j), 1e-4);
  }

}

/*****************************************************************/
/**
 * @brief Unit test based on the { @see Triangle } fixture
 * to test that a triangular heatsource in X with the corresponding
 * initial temperature remains in equilibrium.
 */
TEST_F(Triangle, compute_temperature) {
  
  // Executing one step of the simulation
  heat->compute(system);
  CsvWriter writer("tmp_file");  
  writer.compute(system);

  // test compared to closed form 
  for(auto par = system.begin(); par < system.end(); ++par) {
    UInt i = par->get()->getPosition()[0];
    UInt j = par->get()->getPosition()[1];
    ASSERT_NEAR(static_cast<MaterialPoint&>(*par).getTemperature(), expectedTemp(i, j), 1e-4);
  }

}
