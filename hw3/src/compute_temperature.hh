#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute contact interaction between ping-pong balls
class ComputeTemperature : public Compute {

  // Virtual implementation
public:
  //! Penalty contact implementation
  ComputeTemperature(Real dt);

  //! Constructor for defining specific equation parameters at the initialization
  ComputeTemperature(Real kappa, Real C, Real dt);

  //! Setter for the time constant in the interator
  void setDeltaT(Real dt);

  //! Function to compute the heat equation using FFTW
  void compute(System& system) override;

private:
  Real kappa = 401.0E3; // [W m-1 K-1], Copper heat conductivity x 1000
  Real C = 0.1;         // [J kg-1 K-1]
  Real dt;
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
