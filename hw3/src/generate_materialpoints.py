"""
Small python script to generate material points
on a 512x512 array with a Swiss flag shape.
08.12.2023
"""

import os
import sys

######## PARAMETERS ########
out_path = "./"
out_file = "heat_expl.csv"
N = 512
rho = 8.96E3    # [kn m-3], Copper  density
h_val = 1000
############################
# open file
f = open(os.path.join(out_path, out_file), "w")
print("Write particles in " + f.name)

# write columns names
f.write("# X Y Z VX VY VZ FY FY FZ mass temperature heatrate\n")

# generate particles grid and write line by line
particles = []
for x in range(N):
    for y in range(N) :
        h = 0
        if x >= N/5 and x < 4*N/5 :
            if y >= 2*N/5 and y < 3*N/5 :
                h = h_val
            elif x >= 2*N/5 and x < 3*N/5 :
                if y >= N/5 and y < 4*N/5 :
                    h = h_val
        f.write(str(x) + " " + str(y) + " 0 0 0 0 0 0 0 " + str(rho) + " 20 " + str(h) + "\n")


# clos efile
f.close()

