#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>
/* -------------------------------------------------------------------------- */
ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

ComputeTemperature::ComputeTemperature(Real kappa, Real C, Real dt) : kappa(kappa), C(C), dt(dt) {}

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }

void ComputeTemperature::compute(System& system) {

  // Create one matrix for Particle/Grid and one for the heat source to do FFT afterwards
  UInt N = system.getNbParticles();
  Matrix<complex> grid(std::sqrt(N));
  Matrix<complex> h_v(std::sqrt(N));
  
  // Fill the grid with the temperature values for each particles
  MaterialPoint* matp;

  for(auto& par : system) {
    matp = static_cast<MaterialPoint*>(&par);
    UInt x = matp->getPosition()[1];
    UInt y = matp->getPosition()[0]; 
    grid(x, y) = matp->getTemperature();
    h_v(x, y) = matp->getHeatRate();
  }

  Matrix<complex> GRID = FFT::transform(grid);
  Matrix<complex> H_v = FFT::transform(h_v);
  auto freq = FFT::computeFrequencies(std::sqrt(N));
  
  // For each particle in the Fourier space, compute the heat equation
  // theta^hat_dot_n = (h^hat_v - k * theta^hat_n *(qx^2 + qy^2)/(rho *C)
  for(auto& par : system) {
    matp = static_cast<MaterialPoint*>(&par);
    UInt x = matp->getPosition()[0];
    UInt y = matp->getPosition()[1];
    Real rho = matp->getMass();

    double qx = (double) std::real(freq(x, y))/std::sqrt(N);
    double qy = (double) std::imag(freq(x, y))/std::sqrt(N);

    GRID(x, y) = (H_v(x,y) - kappa * GRID(x, y) * (qx*qx + qy*qy))/(rho * C);
  }

  grid = FFT::itransform(GRID);

  // Update the values of theta_n to the MaterialPoint
  // theta_n+1 = theta_n + dt * theta_dot_n
  for(auto& par : system) {
    matp = static_cast<MaterialPoint*>(&par);
    UInt x = matp->getPosition()[0];
    UInt y = matp->getPosition()[1]; 
    matp->getTemperature() += std::real(grid(x, y)) * dt;  
  }
}

/* -------------------------------------------------------------------------- */
