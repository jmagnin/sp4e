#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

/**
 * @brief Wrapper of the FFTW3 library.
*/
struct FFT {

  /**
   * @brief Performs a 2D DFT on a @ref Matrix of
   * @ref Complex numbers using the FFTW3 api.
   * 
   * @param m : Reference to a Matrix<complex> to be transformed in 2D.
   * @returns  : New Matrix<complex> object contsining the DFT.
  */
  static Matrix<complex> transform(Matrix<complex>& m);

  /**
   * @brief Performs a normalized 2D inverse DFT on a
   * @ref Matrix of @ref Complex numbers
   * using the FFTW3 api.
   * 
   * @param m : Reference to a Matrix<complex> to be transformed in 2D.
   * @returns : New Matrix<complex> object contsining the normalized IDFT.
  */
  static Matrix<complex> itransform(Matrix<complex>& m);

  /**
   * @brief Computes the integer frequency numbers for the given size.
   * To normalize the frequency number, simply divide by the given size.
   * 
   * @param size :  size of the matrix (i.e. nb of rows or columns) that
   *                is to be transformed.
   * @returns :     New Matrix<complex<int>> object containing the frequency
   *                number (integer) of the X dimension in the real part of
   *                each @ref complex element of the @ref Matrix and the
   *                Y dimension in the imaginary part.
  */
  static Matrix<std::complex<int>> computeFrequencies(int size);
};

#endif  // FFT_HH
